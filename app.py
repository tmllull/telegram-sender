from dotenv import dotenv_values
import asyncio
import argparse
import telegram
import os

# Check if env variables are setted
try:
    TELEGRAM_TOKEN = os.environ["TELEGRAM_TOKEN"]
    TELEGRAM_CHAT_ID = os.environ["TELEGRAM_CHAT_ID"]
except Exception:
    # Load .env
    config = dotenv_values(".env")
    TELEGRAM_TOKEN = config["TELEGRAM_TOKEN"]
    TELEGRAM_CHAT_ID = config["TELEGRAM_CHAT_ID"]

parser = argparse.ArgumentParser()

parser.add_argument("-m", "--message", dest="message",
                    default=None, help="The message (string) you want to send")
parser.add_argument("-mf", "--message-from-file", dest="message_from_file",
                    default=None, help="Path to file from where read the message content")
parser.add_argument("-f", "--files", dest="files",
                    default="", help="Path to files. Comma separated")

args = parser.parse_args()

if args.message is not None and args.message_from_file is not None:
    print("Only 1 type of message are allowed")
    parser.print_usage()
    exit()
if args.message is None and args.message_from_file is None and args.files == "":
    print("You must set at least a message or file")
    parser.print_usage()
    exit()

if args.message is not None:
    MESSAGE = args.message

elif args.message_from_file is not None:
    MSG = open(args.message_from_file)
    MESSAGE = MSG.read()
else:
    MESSAGE = ""

FILES = args.files.split(",")


async def main():
    bot = telegram.Bot(TELEGRAM_TOKEN)
    async with bot:
        if FILES[0] != "":
            for path in FILES:
                await bot.sendDocument(chat_id=TELEGRAM_CHAT_ID, document=open(path, 'rb'), caption=MESSAGE)
        elif MESSAGE != "":
            await bot.send_message(text=MESSAGE, chat_id=TELEGRAM_CHAT_ID, parse_mode=telegram.constants.ParseMode.MARKDOWN)
    print("Message sended successfully!")


if __name__ == '__main__':
    asyncio.run(main())
