Send a message (and/or files) to Telegram using Docker (or not). If you send a message with MD format, this will be applied.

1. Create .env file:

```
TELEGRAM_TOKEN=
TELEGRAM_CHAT_ID=
```

2. Run:

```shell
docker build --tag telegram-sender .
```

3. Run:

```shell
docker run -it --env-file /path/to/env/file/.env -v /path/to/message/file:/app --rm telegram-sender app.py [ARGS]
```

If you want to run with a cron job, use:

```shell
docker run --env-file /path/to/env/file/.env -v /path/to/message/file:/app --rm telegram-sender app.py [ARGS]
```

ARGS:

```shell
-h, --help          show this help message and exit
-m, --message            The message (string) you want to send. Required if not use '-message-from-file' arg
-mf, --message-from-file  Path to file from where read the message content. Required if not use '-message' arg
-f, --files              Path to files. Comma separated. Optional
```

4. If you want to run the script without Docker:

```shell
pip install -r requirements.txt
python app.py [ARGS]
```
